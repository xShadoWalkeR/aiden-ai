﻿/// <summary>
/// The class that initiates the AI
/// </summary>
public class G02AIDEN : AIPlayer
{
    /// <summary>The display AI's name</summary>
    public override string PlayerName => "AIDEN";

    /// <summary>
    /// The reference to the AI's thinker class
    /// which implements the 'IThinker' interface
    /// </summary>
    public override IThinker Thinker => thinker;
    private IThinker thinker;

    /// <summary>
    /// Initiates tha AI
    /// </summary>
    public override void Setup()
    {
        thinker = new G02AIDENThinker();
    }
}
