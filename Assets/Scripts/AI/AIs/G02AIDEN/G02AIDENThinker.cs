﻿using System.Collections;
using System.Threading;
using System;

/// <summary>
/// The class that makes the AI calculate its next move
/// </summary>
public class G02AIDENThinker : IThinker
{
    /// <summary>The max possible depth</summary>
    private readonly int maxDepth = 4;

    /// <summary>The current depth</summary>
    private int depth = 1;

    /// <summary>The move the AI will make</summary>
    FutureMove futureMove;

    // Array of ints for the exploration order
    int[] columnOrder;

    /// <summary>
    /// The Think main method for the AI
    /// </summary>
    /// <param name="board">The board</param>
    /// <param name="ct">If there's a cancellation token</param>
    /// <returns>The AI's move</returns>
    public FutureMove Think(Board board, CancellationToken ct)
    {
        // Initialize the array with the same size as the board width
        columnOrder = new int[board.cols];

        // Move exploration order
        for (int i = 0; i < board.cols; i++)
        {
            // Order the columns on the board to get the best order possible for exploration
            columnOrder[i] = board.cols / 2 + (1 - 2 * (i % 2)) * (i + 1) / 2;
        }

        // Start the future move as no move
        futureMove = FutureMove.NoMove;

        // Start the NegaMax to get the movement
        NegaMax(board, ct, int.MinValue, int.MaxValue);

        // If there's a cancelation requested returns no future move
        if (ct.IsCancellationRequested) return FutureMove.NoMove;

        // Return the final move
        return futureMove;
    }

    /// <summary>
    /// Runs the negamax
    /// </summary>
    /// <param name="board">The current board</param>
    /// <param name="ct">If there's a cancellation token</param>
    /// <param name="alpha">The alpha</param>
    /// <param name="beta">The beta</param>
    /// <returns>The best score from the search</returns>
    private int NegaMax(Board board, CancellationToken ct, int alpha, int beta)
    {
        // Returns immediately if there's a cancellation request
        if (ct.IsCancellationRequested) return 0;

        // Starts the best score with the lowest possible value
        int bestScore = int.MinValue;

        // If we've reached max depth or there's a winner, returns the best score
        if (depth >= maxDepth || board.CheckWinner() != Winner.None) return bestScore;

        // Set the max to half the size of the board area
        int max = (board.rows * board.cols - 1) / 2;

        // If beta is greater than max
        if (beta > max)
        {
            // Equals beta to max
            beta = max;

            // If alpha is greater or equal to beta return beta
            if (alpha >= beta) return beta;
        }

        // Go through every column on the board
        for (int i = 0; i < board.cols; i++)
        {
            // If the current column is full, skip to the next one
            if (board.IsColumnFull(columnOrder[i])) continue;

            // Holds the score for the round shapes
            int roundScore = 0;

            // Holds the score for the square shapes
            int squareScore = 0;

            // Holds the shape to be played
            PShape playShape;

            // If we still have square pieces
            if (board.PieceCount(board.Turn, PShape.Square) != 0)
            {
                // Do a move with a square piece
                board.DoMove(PShape.Square, columnOrder[i]);

                // Increase the depth
                depth++;

                // Set the square score to be the inverted negamax score plus the score from the heuristics
                squareScore = -NegaMax(board, ct, -beta, -alpha) + Heuristics(board);

                // Decrease the depth
                depth--;

                // Undo the move
                board.UndoMove();
            }

            // If we still have round pieces
            if (board.PieceCount(board.Turn, PShape.Round) != 0)
            {
                // Do a move with a round piece
                board.DoMove(PShape.Round, columnOrder[i]);

                // Increase the depth
                depth++;

                // Set the round score to be the inverted negamax score plus the score from the heuristics
                roundScore = -NegaMax(board, ct, -beta, -alpha) + Heuristics(board);

                // Decrease the depth
                depth--;

                // Undo the move
                board.UndoMove();
            }

            // If the square score or the round score are greater than the current best score
            if (squareScore > bestScore || roundScore > bestScore)
            {
                // Set the best score to be equal to the highest of the two
                bestScore = Math.Max(squareScore, roundScore);

                // Set the play shape to the equal to the one who had the higher score
                playShape = squareScore > roundScore ? PShape.Square : PShape.Round;

                // If we have 1 or less round pieces
                if (board.PieceCount(board.Turn, PShape.Round) <= 1)
                {
                    // Sets the play shape to be a square
                    playShape = PShape.Square;
                }
                // Else if we have 1 or less square pieces
                else if (board.PieceCount(board.Turn, PShape.Square) <= 1)
                {
                    // Sets the play shape to be round
                    playShape = PShape.Round;
                }

                // Returns the future move, also taking into account the amount of square and round pieces left
                futureMove = new FutureMove(
                    columnOrder[i],
                    board.PieceCount(board.Turn, playShape) == 0 ?
                    playShape == PShape.Round ?
                    PShape.Square : PShape.Round : playShape);
            }

            // Check if the best score is greater or equal to beta; If so return `bestScore`
            if (bestScore >= beta) return bestScore;

            // If the best score is greater than alpha
            if (bestScore > alpha) 
            {
                // Set the alpha equal to the best score
                alpha = bestScore;
            }
        }

        // Returns alpha
        return alpha;
    }

    /// <summary>
    /// Heuristics to atribute score to a play
    /// </summary>
    /// <param name="board">The board</param>
    /// <returns>The score based on the quality of the move</returns>
    private int Heuristics(Board board)
    {
        // Final score starts at 0
        int finalScore = 0;

        // Holds the current piece and color
        PShape? currentPiece = PShape.Round;
        PColor? currentColor = PColor.White;

        // Holds the current number of pieces in sequence
        int colorSequence;
        int pieceSequence;

        // Goes through every corridor in the winCorridors variable
        foreach (IEnumerable corridor in board.winCorridors)
        {
            // Reset the color and piece sequence to 1
            colorSequence = 1;
            pieceSequence = 1;

            // Goes through every position in the corridor
            foreach (Pos pos in corridor)
            {
                // Verify if the current position has a piece
                if (board[pos.row, pos.col].HasValue)
                {
                    // If the shape on this position is the same as the current piece
                    if (board[pos.row, pos.col].Value.shape == currentPiece)
                    {
                        // Increase the sequence by 1
                        pieceSequence++;
                    } 
                    // Else if we have the correct number of pieces in sequence and
                    // our color matches the shape
                    else if (pieceSequence >= board.piecesInSequence - 3 &&
                        (board.Turn == PColor.White && currentPiece == PShape.Round ||
                        board.Turn == PColor.Red && currentPiece == PShape.Square))
                    {
                        // Increase the score by 150
                        finalScore += 150;
                    }
                    // Else if we have the correct number of pieces
                    else if (pieceSequence >= board.piecesInSequence - 3)
                    {
                        // Set the current piece
                        currentPiece = board[pos.row, pos.col].Value.shape;
                        
                        // Increase the final score by 100
                        finalScore += 100;

                        // Reset the piece sequence to 1
                        pieceSequence = 1;
                    } 
                    // Else
                    else
                    {
                        // Set the current piece
                        currentPiece = board[pos.row, pos.col].Value.shape;

                        // Reset the piece sequence to 1
                        pieceSequence = 1;
                    }

                    // If the board color is the same as the current color
                    if (board[pos.row, pos.col].Value.color == currentColor)
                    {
                        // Increase the color sequence
                        colorSequence++;
                    } 
                    // Else if the color sequence is the correct number and 
                    // the turn is equal to the current color
                    else if (colorSequence >= board.piecesInSequence - 3 && 
                        board.Turn == currentColor)
                    {
                        // Increase the final score by 150
                        finalScore += 150;
                    }
                    // Else if the color sequence is the correct number
                    else if (colorSequence >= board.piecesInSequence - 3)
                    {
                        // If the piece sequence is the correct number and the shape is the same
                        if (currentPiece == board[pos.row, pos.col].Value.shape && pieceSequence >= 2)
                        {
                            // Set the current color 
                            currentColor = board[pos.row, pos.col].Value.color;

                            // Reset the color sequence to 1
                            colorSequence = 1;
                        } 
                        // Else
                        else
                        {
                            // Set the current color
                            currentColor = board[pos.row, pos.col].Value.color;

                            // Increase the final score by 50
                            finalScore += 50;

                            // Reset the color sequence to 1
                            colorSequence = 1;
                        }
                    } 
                    // Else
                    else
                    {
                        // Set the current color
                        currentColor = board[pos.row, pos.col].Value.color;

                        // Reset the color sequence to 1
                        colorSequence = 1;
                    }
                } else
                {
                    // Reset the color and piece sequence to 0
                    colorSequence = 0;
                    pieceSequence = 0;
                }
            }
        }

        // Depending on the depth returns the score or the inverted score
        if (depth % 2 == 0)
        {
            // Returns the final score
            return finalScore;
        } else
        {
            // Returns the inverted final score
            return -finalScore;
        }
    }
}
