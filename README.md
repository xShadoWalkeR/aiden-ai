# __AIDEN__

An assignment for the Artificial Intelligence course unit of the [Bachelor in
Videogames][licvideo] at [Universidade Lusófona de Humanidades e
Tecnologias][ULHT], Lisbon, Portugal.

## Authorship
* [ __Flávio Santos__](https://github.com/fs000) > a21702344
* [ __Leandro Brás__](https://github.com/xShadoWalkeR) > a21801026
* [ __Sara Gama__](https://github.com/serapinta) > a21705494

## Remote Repository
[__GitLab__](https://gitlab.com/xShadoWalkeR/aiden-ai)

### Assignment tasks
__Flávio Santos__ > Created the first Negamax implementation for the game (with
a poor heuristic), made the diagram and helped with the main Thinker class and
this report.

__Leandro Brás__ > Created the main Thinker class and helped with the diagram
and this report.

__Sara Gama__ > Created the class for the AI initiation and made this report.

## Project Architecture

### Solution
This project is based on the game [*Simplexity*](simplexity) designed by
[*Brain Blender Games*](brain).
The AI is composed by 2 classes: [`G02AIDEN`] and [`G02AIDENThinker`].
The `G02AIDEN` class (which extends [`AIPlayer`]) is used to instantiate the
`G02AIDENThinker` class and to set the AI's name.
The `G02AIDENThinker` class (which implements [`IThinker`]) has 3 main methods:

The `Think()` method accepts a (copy of the) `Board` and a `CancellationToken`.
It starts by defining an array of `int` which later gets filled in a `for` loop
that defines the AI's columns ordering preference by a static function which
*zig zags* from the middle column. After that, `Negamax()` gets called to start
the AI's decision making process.
If too much time goes by, no move is performed and the AI forfeits the match.

The `NegaMax()` method returns a score based on the selected position.
It has a `Board`, `CancellationToken`, and two `int` type numbers as parameters.
The algorithm used here is *Negamax with alpha beta pruning, move ordering and
deepening*, which means it will select a position a lot faster than the regular
Negamax. Firstly, the method creates an `int` named `bestScore` with its value
set to the lowest `int` value possible. Afterwards, the current AI's `depth` is
compared to its max value (`maxDepth`) and the game verifies if there is any
`Winner`. If either of these two conditions is true, the `bestScore` gets
returned immediately.
After that, a `max` value is set and with the `alpha`'s value, gets compared to
the current `beta` in order to help define (and control) it. Lastly in this
method there is a `for` loop which checks all columns by the prefered order.
In this loop, if a column is detected as full, then it's skipped and we move
towards the next. If not, two scores are set to `0` and a new variable to hold
a piece shape (`PShape`) is created. The AI checks if there are still pieces of
a certain shape and if so we perform a move on the currently selected column
with that piece shape. The depth is increased and a variable holding a score of
that piece type is set as the sum of the inverted `Negamax()` method and the
`Heuristics()` method, switching the `alpha` and `beta` values when the
`Negamax()` gets called. The depth is decreased and the previous move is undone
(this is done for each shape type, seeing as any player can play any shape).
The AI then checks if either of the last scores are greater than the current
`bestScore` and if so the `bestScore` is updated to be the greater of the two
values. The best shape (`playShape`) is changed to represent the shape with he
highest score. A check is then done to know if there are still round or square
pieces available, and if not, the current used piece is replaced by its opposite.
The `futureMove` variable is set to a new `FutureMove` with the current column
and `playShape`.

Lastly, the AI checks if the `bestScore` is greater or equal to the current
`beta` value and if so, its best score is returned. Then, it verifies if the
`bestScore` is greater than alpha, and if so, the AI sets the alpha to have
its value, returning the alpha.

In the `Heuristics()` method, when a shape or color gets found on a corridor,
a counter gets increased. If the next position checked is empty, the counter
is reset and if the next position checked has a shape different from the previous
shape and a color different from the previous color and counter has at least a
value of 2, the final score is increased, the counter gets reset and the AI saves
the shape of the current shape and color of the piece. According to its depth level,
the final score can be returned as inverted or normal.

### Diagrams
![AIDEN_Flowchart](AIDEN_Flowchart.png)

## References
* Powerpoints provided by [Nuno Fachada](https://github.com/fakenmc)
* [SOLVING CONNECT 4](http://blog.gamesolver.org/)
*  Millington, Ian. (2019). _AI for GAMES, Trird Edition_, U.S. , CRC Press.

[licvideo]:https://www.ulusofona.pt/en/undergraduate/videogames
[ULHT]:https://www.ulusofona.pt/
[brain]:https://boardgamegeek.com/boardgamedesigner/35066/brain-bender-games
[simplexity]:https://boardgamegeek.com/boardgame/55810/simplexity
[`AIPlayer`]:Assets/Scripts/AI/AIPlayer.cs
[`IThinker`]:Assets/Scripts/AI/IThinker.cs
[`G02AIDEN`]:Assets/Scripts/AI/AIs/G02AIDEN/G02AIDEN.cs
[`G02AIDENThinker`]:Assets/Scripts/AI/AIs/G02AIDEN/G02AIDENThinker.cs
